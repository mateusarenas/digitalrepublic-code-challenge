import React from "react";

import { v4 as uuid } from 'uuid';

import { useLocalStorage } from 'usehooks-ts';

import { Wall, Openning, Paint } from '../types';

export interface WallCalculatorContextData {
  walls: Wall[]
  getWall(id: string): Wall | null
  storeWall(wall: Wall): void
  delWall(id: string): void
  opennings: Openning[]
  getOpenning(id: string): Openning | null
  storeOpenning(Openning: Openning): void
  delOpenning(id: string): void
  paint: Paint | null
  setPaint: React.Dispatch<React.SetStateAction<Paint | null>>
}

const WallCalculatorContext = React.createContext<WallCalculatorContextData>({} as WallCalculatorContextData)

interface WallCalculatorProviderProps { children: React.ReactNode }

export const WallCalculatorProvider: React.FC<WallCalculatorProviderProps> = ({ children }) => {
  // const [walls, setWalls] = useLocalStorage<Wall[]>('@1walls', []);
  const [walls, setWalls] = React.useState<Wall[]>([]);
  // const [opennings, setOpennings] = useLocalStorage<Openning[]>('@1opennings', []);
  const [opennings, setOpennings] = React.useState<Openning[]>([]);
  const [paint, setPaint] = React.useState<Paint | null>(null);

  function storeWall(wall: Wall) {
    setWalls(walls => [...walls, { ...wall, id: uuid() }])
  }

  function delWall(id: string) {
    setWalls(walls => walls.filter(wall => wall.id !== id))
  }

  function getWall(id: string) {
    return walls.find(wall => wall.id === id) ?? null;
  }

  function storeOpenning(openning: Openning) {
    setOpennings(opennings => [...opennings, { ...openning, id: uuid() }])
  }

  function delOpenning(id: string) {
    setOpennings(opennings => opennings.filter(openning => openning.id !== id))
  }

  function getOpenning(id: string) {
    return opennings.find(openning => openning.id === id) ?? null;
  }

  return (
    <WallCalculatorContext.Provider value={{
      walls,
      storeWall,
      delWall,
      getWall,
      opennings,
      storeOpenning,
      delOpenning,
      getOpenning,
      paint, setPaint
    }} >
      {children}
    </WallCalculatorContext.Provider>
  )
}

export default WallCalculatorContext