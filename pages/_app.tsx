import { WallCalculatorProvider } from '../contexts/wallcalculator'

import '../styles/globals.css'

import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WallCalculatorProvider>
      <Component {...pageProps} />
    </WallCalculatorProvider>
  )
}

export default MyApp
