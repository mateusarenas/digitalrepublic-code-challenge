// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import WallCalculator from '../../services/WallCalculator';

type Body = {
  walls: { height: number; width: number; area: number }[];
  doors_walls: number[];
  windows_walls: number[];
};

type Data = {
  data?: any;
  error?: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  try {
    const payload: Body = req.body;

    const walls = payload.walls.map(({ width, height, ...rest }) => {
      const area = WallCalculator.calculateWallArea(width, height);

      return { ...rest, width, height, area };
    });

    return res.status(200).json({ data: walls });
  } catch (error) {
    return res.status(400).json({
      error: (error as Error).message,
    });
  }
}
