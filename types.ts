export type Wall = {
  id: string;
  height: number;
  width: number;
  area: number;
};

export type Openning = {
  id: string;
  type: string;
  height: number;
  width: number;
  area: number;
  wall: string;
};

export type Paint = {
  liters: number;
  area: number;
  cans: { size: number; quantity: number }[];
};
