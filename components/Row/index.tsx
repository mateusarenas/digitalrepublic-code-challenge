import React from 'react';

interface RowProps {
  style?: React.CSSProperties
  children?: React.ReactNode
}

function Row({ children, style }: RowProps) {
  return (
    <div style={{ ...{ display: 'flex', flexDirection: 'row' }, ...style }}>
      {children}
    </div>
  )
}


interface RowColumnProps {
  style?: React.CSSProperties
  children?: React.ReactNode
  col?: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '12' | '11' | '12'
  justifyContent?: React.CSSProperties['justifyContent']
  alignItems?: React.CSSProperties['alignItems']
}

const RowColumn: React.FC<RowColumnProps> = ({ style = {}, children, col = 12, justifyContent, alignItems }) => {

  const flexBasis = [
    "8.33%",
    "16.67%",
    "25%",
    "33.33%",
    "41.67%",
    "50%",
    "58.33%",
    "66.67%",
    "75%",
    "83.33%",
    "91.67%",
    '100%',
  ]

  return (
    <div style={{ ...{ display: 'flex', flexBasis: flexBasis[+col], justifyContent, alignItems }, ...style }}>
      {children}
    </div>
  )
}

Row.Column = RowColumn;

Row.Column.displayName = 'Row.Column';

export default Row;