import React from 'react';

import styles from './styles.module.scss';

interface SubmitProps {
  children?: React.ReactNode
}

const Submit: React.FC<SubmitProps> = ({ children }) => {
  return (
    <button type="submit" className={styles['btn-primary']}>{children}</button>
  )
}

export default Submit;