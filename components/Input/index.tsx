import React from 'react';
import { Controller, FieldValues, ValidationRule, useController, useForm, useFormContext } from 'react-hook-form';
import InputMask from 'react-input-mask';

import styles from './styles.module.scss';


interface InputProps {
  label?: string
  name: string
  type?: 'text' | 'hidden' | 'metters'
  value?: any
  required?: boolean
  disabled?: boolean
}

type patterns = {
  [key in keyof InputProps]?: ValidationRule<RegExp>
}

const patterns = {
  'text': undefined,
  'hidden': undefined,
  'metters': {
    value: /^(?:[1-4]?\d|50)(?:\.\d{1,2})?$/,
    message: "A largura deve ser entre 1 e 50 metros.",
  },
}

function Input({ name, label, required, type = 'text', value, disabled }: InputProps) {

  const { control } = useFormContext<FieldValues>();

  const { field, fieldState: { error } } = useController<FieldValues>({
    name,
    control,
    defaultValue: value,
    shouldUnregister: true,
    rules: {
      required: required ? "Este campo é obrigatório." : false,
      pattern: patterns[type],
    }
  });

  if (type === "hidden") return null;

  return (
    <div className={styles['floating-label-content']}>

      <InputMask
        className={styles['floating-input']}
        id={field.name}
        mask="99.99"
        maskChar={null}
        placeholder="Digite a largura da parede"
        onChange={field.onChange}
        value={field.value}
        onBlur={field.onBlur}
        disabled={disabled}
      />

      <label className={styles['floating-label']} htmlFor={name}>{label}</label>
      {!!error && <span>{error?.message?.toString()}</span>}
    </div>
  )
}

interface InputGroupProps {
  children?: React.ReactNode
}

const InputGroup: React.FC<InputGroupProps> = ({ ...rest }) => {
  return <div className={styles.inputGroup} {...rest} />;
};

Input.Group = InputGroup;

Input.Group.displayName = 'Input.Group';

export default Input;