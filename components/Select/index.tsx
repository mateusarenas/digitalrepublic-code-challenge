import React from 'react';
import { FieldValues, useController, useFormContext } from 'react-hook-form';

import styles from './styles.module.scss';

interface SelectProps {
  label?: string
  name: string
  value?: any
  required?: boolean
  children?: React.ReactNode
  onChange?: (value: string) => any
}

function Select({ label, name, required, value, children, onChange }: SelectProps) {

  const { control } = useFormContext<FieldValues>();

  const { field, fieldState: { error } } = useController<FieldValues>({
    name,
    control,
    defaultValue: value,
    shouldUnregister: true,
    rules: {
      required: required ? "Este campo é obrigatório." : false,
    }
  });

  const data = React.useMemo(() => {
    const values: SelectOptionProps[] = React.Children.map(children, (child: any, index) => ({
      label: child?.props?.children,
      value: child?.props?.value,
      selected: child?.props?.selected,
    }))
    return values;
  }, [children]);

  const handleChange = (value: any) => {
    field.onChange(value);
    onChange?.(value)
  };

  React.useEffect(() => {
    if (data.length) {
      const selected = data.find(item => item.selected);
      if (selected) handleChange(selected.value);
      else handleChange(data[0].value);
    }
  }, [])

  return (
    <div>
      <div className={styles['floating-label-content']}>
        <select className={styles["floating-select"]}
          value={field.value}
          onChange={event => handleChange(event.target.value)}
        >
          {data.map((item, index) => <option key={index} {...item} />)}
        </select>
        <label className={styles['floating-label']}>{label}</label>
      </div>
      {!!error && <span>{error?.message?.toString()}</span>}
    </div>
  )
}

interface SelectOptionProps {
  children?: string
  value?: string
  selected?: boolean
}

const SelectOption: React.FC<SelectOptionProps> = ({ ...rest }) => {
  return <option {...rest} />;
};

Select.Option = SelectOption;

Select.Option.displayName = 'Select.Option';

export default Select;