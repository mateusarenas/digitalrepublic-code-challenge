import React from 'react';
import { FieldValues, FormProvider, FormProviderProps, SubmitHandler } from 'react-hook-form';

import styles from './styles.module.css';

interface FromProps<T extends FieldValues = any> extends FormProviderProps<T> {
  onSubmit: SubmitHandler<T>
}

function Form({ handleSubmit, onSubmit, children, ...rest }: FromProps) {

  return (
    <FormProvider {...rest} handleSubmit={handleSubmit} >
      <form className={styles.formControl} onSubmit={handleSubmit(onSubmit)}>
        {children}
      </form>
    </FormProvider>
  )
}

interface FormGroupProps {
  children?: React.ReactNode
}

const FormGroup: React.FC<FormGroupProps> = ({ ...rest }) => {
  return <div className={styles.formGroup} {...rest} />;
};

Form.Group = FormGroup;

Form.Group.displayName = 'Form.Group';

export default Form;