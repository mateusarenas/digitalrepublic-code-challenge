import React from 'react';
import { IoCloseCircle } from 'react-icons/io5';

interface CardProps {
  style?: React.CSSProperties
  children?: React.ReactNode
}

function Card({ style = {}, children }: CardProps) {

  return (
    <div style={{
      ...{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' },
      ...{ padding: 16 },
      ...style
    }}>
      {children}
    </div>
  );
}


interface CardCloseButtonProps {
  onPress?: () => any
  style?: React.CSSProperties
}

const CardCloseButton: React.FC<CardCloseButtonProps> = ({ style = {}, onPress }) => {
  return (
    <button style={{ ...{ marginRight: 16 }, ...style }} onClick={onPress}>
      <IoCloseCircle size={32} color='red' />
    </button>
  )
}

Card.CloseButton = CardCloseButton;

Card.CloseButton.displayName = 'Card.CloseButton';


interface CardContentProps {
  style?: React.CSSProperties
  children?: React.ReactNode
}

const CardContent: React.FC<CardContentProps> = ({ style = {}, children }) => {
  return (
    <div style={{
      ...{ display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center' },
      ...style
    }}
    >
      {children}
    </div>
  )
}

Card.Content = CardContent;

Card.Content.displayName = 'Card.Content';

interface CardCellProps {
  style?: React.CSSProperties
  children?: React.ReactNode
}

const CardCell: React.FC<CardCellProps> = ({ style = {}, children }) => {
  return (
    <span style={{
      ...{ display: 'flex', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
      ...style
    }}
    >
      {children}
    </span>
  )
}

Card.Cell = CardCell;

Card.Cell.displayName = 'Card.Cell';

export default Card;