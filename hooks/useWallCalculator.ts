import React from 'react';
import WallCalculatorContext, {
  WallCalculatorContextData,
} from '../contexts/wallcalculator';

export default function useWallCalculator(): WallCalculatorContextData {
  const methods = React.useContext(WallCalculatorContext);

  return methods;
}
