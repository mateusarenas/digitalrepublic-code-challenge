export default class WallCalculator {
  /**
   * Este método recebe a largura e a altura de uma parede e calcula a sua área.
   * Se a área resultante for menor que 1 ou maior que 50, ele lança um erro.
   * @param width
   * @param height
   * @returns
   */
  static calculateWallArea(width: number, height: number) {
    const area = Number(width) * Number(height);
    if (area < 1 || area > 50) {
      throw new Error('A parede deve ter entre 1 e 50 metros quadrados.');
    }
    return area;
  }

  /**
   * Este método retorna a largura e altura de uma porta, que é fixa em 0,8m x 1,9m.
   * @returns: { width: 0.8, height: 1.9 }
   */
  static getDoorSizes() {
    return { width: 0.8, height: 1.9 };
  }

  /**
   * Este método retorna a área de uma porta, que é fixa em 0,8m x 1,9m.
   * @returns
   */
  static calculateDoorArea() {
    return 0.8 * 1.9;
  }

  /**
   * Este método retorna a largura e altura de uma janela, que é fixa em 2m x 1,2m.
   * @returns: { width: 2, height: 1.2 }
   */
  static getWindowSizes() {
    return { width: 2, height: 1.2 };
  }

  /**
   * Este método retorna a área de uma janela, que é fixa em 2m x 1,2m.
   * @returns
   */
  static calculateWindowArea() {
    return 2 * 1.2;
  }

  /**
   * Este método recebe o número de portas e o número de janelas
   * em uma parede e calcula a área total ocupada por elas.
   * @param numDoors
   * @param numWindows
   * @returns
   */
  static calculateTotalDoorWindowArea(numDoors: number, numWindows: number) {
    const doorArea = Number(numDoors) * WallCalculator.calculateDoorArea();
    const windowArea =
      Number(numWindows) * WallCalculator.calculateWindowArea();
    return doorArea + windowArea;
  }

  /**
   * Este método recebe a área total da parede, o número de portas e o número de janelas,
   * e verifica se a área total ocupada pelas portas e janelas é menor ou igual a 50% da área da parede.
   * Se for maior, ele lança um erro.
   * @param wallArea
   * @param numDoors
   * @param numWindows
   * @returns
   */
  static checkDoorWindowAreaLimit(
    wallArea: number,
    numDoors: number,
    numWindows: number
  ) {
    const totalDoorWindowArea = WallCalculator.calculateTotalDoorWindowArea(
      numDoors,
      numWindows
    );
    if (totalDoorWindowArea > Number(wallArea) / 2) {
      throw new Error(
        'A área total das portas e janelas não pode ser maior do que 50% da área da parede.'
      );
    }
  }

  /**
   * Este método recebe a altura de uma porta e retorna a altura da parede levando em conta essa porta.
   * A altura é aumentada em 0,3m para acomodar a caixa do batente da porta.
   * @param doorHeight
   * @returns
   */
  static calculateWallHeightWithDoor(doorHeight: number) {
    return Number(doorHeight) + 0.3;
  }

  /**
   * Este método recebe a altura parede e a altura da porta,
   * e verifica altura da parede com a porta é menor do que o mínimo exigido.
   * Se for maior, ele lança um erro.
   * @param wallHeight
   * @param doorHeight
   * @returns
   */
  static checkWallHeightWithDoor(wallHeight: number, doorHeight: number) {
    if (
      WallCalculator.calculateWallHeightWithDoor(Number(doorHeight)) >
      Number(wallHeight)
    ) {
      throw new Error(
        'A altura da parede com a porta é menor do que o mínimo exigido.'
      );
    }
  }

  /**
   *  Este método recebe a área de uma parede e calcula a quantidade de tinta
   *  necessária para pintá-la. Ele também retorna o tamanho da lata de tinta necessária,
   *  com base na quantidade de tinta necessária. O tamanho da lata pode ser 0,5L, 2,5L, 3,6L ou 18L,
   *  dependendo da quantidade de tinta necessária.
   *
   * @param wallArea
   * @returns
   */
  static calculatePaintQuantity(wallArea: number) {
    const paintLiters = Number(wallArea) / 5;
    const canSize =
      paintLiters <= 0.5
        ? 0.5
        : paintLiters <= 2.5
        ? 2.5
        : paintLiters <= 3.6
        ? 3.6
        : 18;
    return {
      paintLiters: paintLiters,
      canSize: canSize,
    };
  }

  /**
  Converte a área de um espaço em litros de tinta necessários para cobri-la.
  @param {number} area - área em metros.
  @returns {number} A quantidade de litros de tinta necessários para cobrir a área informada.
  */
  static areaToLiters(area: number): number {
    const litersPerSquareMeter = 0.5; // Quantidade de litros necessários por metro quadrado
    const totalLiters = area * litersPerSquareMeter;
    return totalLiters;
  }

  /**
   * Este método retorna um array com os tamanhos disponíveis das latas de tinta, em ordem decrescente.
   * @returns
   */
  static getCanSizes() {
    return [18, 3.6, 2.5, 0.5];
  }

  /**
  Calcula a quantidade e tamanho das latas de tinta necessárias com base nos litros de tinta informados.
  @param {number} liters - A quantidade de tinta necessária em litros.
  @returns {object[]} Um array de objetos contendo o tamanho e a quantidade necessária de cada lata de tinta para cobrir a área de pintura requerida.
  */
  static calculatePaintCans(
    liters: number
  ): { size: number; quantity: number }[] {
    const cans: { size: number; quantity: number }[] = [];

    if (liters <= 0) return cans;

    const canSizes = WallCalculator.getCanSizes();

    for (let i = 0; i < canSizes.length; i++) {
      const canSize = canSizes[i];
      const canQuantity = Math.floor(liters / canSize);

      if (canQuantity > 0) {
        cans.push({ size: canSize, quantity: canQuantity });
        liters -= canQuantity * canSize;
      }

      if (liters <= 0) {
        break;
      }
    }

    if (liters > 0) {
      cans.push({ size: 0.5, quantity: 1 }); // add a 0.5L can if there is leftover paint
    }

    return cans;
  }

  /**
   * Calcula as latas de tinta necessárias com base nos litros de tinta informados e nas latas de tinta disponíveis.
   *
   * @param {number} liters - A quantidade de tinta necessária em litros.
   * @returns {object[]} An array of objects containing the can size and quantity needed to cover the required paint area.
   */
  static calculateRequiredCans(
    liters: number
  ): { size: number; quantity: number }[] {
    const requiredCans = [];
    let remainingLiters = liters;

    const canSizes = WallCalculator.getCanSizes();

    for (let i = 0; i < canSizes.length; i++) {
      const canSize = canSizes[i];

      if (remainingLiters <= 0) {
        break;
      }

      if (remainingLiters >= canSize) {
        const quantity = Math.floor(remainingLiters / canSize);
        requiredCans.push({ size: canSize, quantity });
        remainingLiters -= quantity * canSize;
      }
    }

    if (remainingLiters > 0) {
      requiredCans.push({ size: 0.5, quantity: 1 }); // add a 0.5L can if there is leftover paint
    }

    return requiredCans;
  }
}
